import React, {useEffect, useRef} from 'react';
import {Security} from "@okta/okta-react";
import {oktaAuthConfig, oktaSignInConfig} from "./config";
import {OktaAuth} from "@okta/okta-auth-js";
import OktaSignIn from "@okta/okta-signin-widget";
import '@okta/okta-signin-widget/dist/css/okta-sign-in.min.css';

const oktaAuth = new OktaAuth(oktaAuthConfig);

const App = () => {
  return (
    <Security
      oktaAuth={oktaAuth}
      restoreOriginalUri="/"
    >
      <OktaSignInWidget config={oktaSignInConfig}/>
    </Security>
  );
}

const OktaSignInWidget = ({ config, onSuccess, onError }) => {
  const widgetRef = useRef();
  useEffect(() => {
    if (!widgetRef.current)
      return false;

    const widget = new OktaSignIn(config);

    widget.showSignInToGetTokens({
      el: widgetRef.current,
    }).then(onSuccess).catch(onError);

    return () => widget.remove();
  }, [config, onSuccess, onError]);

  return (<div ref={widgetRef} />);
};

export default App;
